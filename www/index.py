# -*-coding:Latin-1 -*
import cgitb; cgitb.enable()
import cgi
import html
print('Content-type: text/html')
print()
formulaire = cgi.FieldStorage()
if formulaire.getvalue('nom') == None:
    print('Veuillez remplir le formulaire :')
    print('<form action="" method="post">')
    print('<input type="text" name="nom" />')
    print('<input type="submit">')
    print('</form>')
else:
    nom =  html.escape(formulaire.getvalue('nom'))
    print('Ainsi, vous vous appelez', nom,' ?')