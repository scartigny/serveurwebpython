# -*-coding:Latin-1 -*
import http.server
import threading
import webbrowser
import os
import urllib.parse

#Set root directory
os.chdir("./www")

class CGIHTTPRequestHandlerPY(http.server.CGIHTTPRequestHandler):
    def is_cgi(self):
        collapsed_path = http.server._url_collapse_path(urllib.parse.unquote(self.path))
        dir_sep = collapsed_path.find('/', 1)
        self.cgi_info = collapsed_path[:dir_sep], collapsed_path[dir_sep+1:]
        return True
    
    def send_head(self):
        path = self.__ajouterIndexPY()
        if os.path.exists(path): 
            if self.is_python(path):
                return http.server.CGIHTTPRequestHandler.send_head(self)
        return http.server.SimpleHTTPRequestHandler.send_head(self)
    
    def do_POST(self):
        self.__ajouterIndexPY()
        http.server.CGIHTTPRequestHandler.do_POST(self)  

    def __ajouterIndexPY(self):
        path = self.translate_path(self.path)
        if os.path.isdir(path):
            for file in "index.py", :        
                path = os.path.join(path, file)            
                if os.path.exists(path):
                    if not self.path.rstrip().endswith('/'):
                        self.path += '/'                    
                    self.path += file
                    break
        return path

########################################################################       
class monServeur():
    """Serveur web tout simple.
    
    Parametres constructeur
    -----------------------
    - ip : adresse ip qui sera bind ("" par d�faut, soit toutes les interfaces)    
    - port : port qui sera ouvert (80 par d�faut)
    
    """

    #----------------------------------------------------------------------
    def __init__(self, ip = "", port = 80):
        """Constructeur"""
        #threading.Thread.__init__(self) # Mauvaise m�thode car impossible de relancer apr�s arr�t
        self.__message("Serveur web")
        self.ip = ip
        self.port = port
        self.__message("Ip (si blanc alors tous) :", self.ip)
        self.__message("Port :", self.port)   
        self.adresse = (ip, port)
        self.handler = CGIHTTPRequestHandlerPY
        self.enEcoute = False

        # self.shutdownServeur = threading.Event() # Ecoute conditionn�e
        
    def start(self):  # bonne m�thode
        self.thread = threading.Thread(target=self.__run)
        self.thread.start()
        
    def __run(self):
        if not self.enEcoute:
            self.enEcoute = True
            self.__message("> Ecoute sur {}:{}...".format(self.ip, self.port))  
            # self.shutdownServeur.clear() # Ecoute conditionn�e
            # while not self.shutdownServeur.isSet(): # Ecoute conditionn�e
                #self.httpd.handle_request()
            server = http.server.HTTPServer  
            self.httpd = server(self.adresse, self.handler)
            self.httpd.serve_forever() # Ecoute permanente
            self.__message("> Arr�t de l'�coute du port", self.port)
        else:
            self.__message("> Le serveur est d�j� en �coute...") 

    def stop(self, timeout = 3):
        if self.enEcoute:
            self.enEcoute = False
            self.__message("> Arret du serveur...")  
            # self.shutdownServeur.set() # Ecoute conditionn�e
            self.httpd.shutdown() # Ecoute permanente
            self.httpd.socket.close()
            self.__message("> Fermeture")
            self.thread.join(timeout)  #None pour illimit�
        else:
            self.__message("> Le serveur est d�j� � l'arr�t...")
            
    def test(self):
        if self.enEcoute:            
            if self.ip != "":
                ip =  self.ip
            else:
                ip = "127.0.0.1"
            webbrowser.open('http://{}:{}/'.format(ip, self.port))
        else:
            self.__message("> Le serveur n'est pas � l'�coute...")
            
    def status(self):
        if self.enEcoute:            
            self.__message("> Status : Le serveur est pas � l'�coute.") 
        else:
            self.__message("> Status : Le serveur n'est pas � l'�coute.")                    
        
    def __message(self, *parametres):
        print("Serveur WEB#", *parametres)        

serveur = monServeur(port = 10001)
def aide():
    print("-> start : D�marre le serveur")
    print("-> stop : Arr�te le serveur")
    print("-> status : Retourne l'�tat d'�coute du serveur")
    print("-> test: Ouvre un navigateur")    
    print("-> quit : Quitte l'application")
    print("-> help : Affiche ce menu")
serveurMenu = {
    'start': serveur.start,
    'stop': serveur.stop,
    'test': serveur.test,
    'status': serveur.status,    
    'help': aide,
}

serveur.start()
saisie = "" 
while saisie != 'quit':
    saisie = input("Que voulez-vous faire (help pour l'aide) ? ").lower()
    try:
        choix = serveurMenu[saisie]
    except KeyError:
        pass    
    else:
        choix()
serveur.stop()        