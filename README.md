# Petit serveur web sous console capable d’interpréter les fichiers .py
Compatible Mac / Windows / Linux #

## Fichiers ##

* www : répertoire racine de vos fichiers web
* \__init__.py : package python
* main.py : script python à lancer pour exécuter le serveur 
* class.png : diagramme de classe

## Diagramme de classe ##

![class.png](https://bitbucket.org/repo/5yRAEo/images/3069162504-class.png)